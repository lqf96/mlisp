module MLisp.Compiler.Types where

import Data.Map (Map)
import Data.Vector (Vector)
import Control.Monad.State (State)
import Control.Monad.Except (ExceptT)

-- [ Data Types ]
-- Label type
type MLispLabel = (Int, Int)

-- MLisp instruction type
data MLispInst =
    -- Movement instructions
    Rrmov { fromReg :: Int, toReg :: Int } |
    Rmmov { fromReg :: Int, toVarReg :: Int } |
    Mrmov { fromVarReg :: Int, toReg :: Int } |
    -- Immediate movement instructions
    Irmovn { toReg :: Int } |
    Irmovi { intVal :: Integer, toReg :: Int } |
    Irmovb { boolVal :: Bool, toReg :: Int } |
    Irmovc { charVal :: Char, toReg :: Int } |
    Irmovs { strVal :: String, toReg :: Int } |
    Irmovr { intVal1 :: Integer, intVal2 :: Integer, toReg :: Int } |
    -- Subroutine instructions
    Call { nArgs :: Int, baseReg :: Int } |
    Ret { retValReg :: Int } |
    -- Jump instructions (With intermediate form)
    Jmp { addr :: Int } |
    Ijmp { label :: MLispLabel } |
    Cj { condReg :: Int, addr :: Int } |
    Icj { condReg :: Int, label :: MLispLabel } |
    -- High-order function instructions (With intermediate form)
    Mkfunc { nArgs :: Int, addr :: Int, toReg :: Int } |
    Imkfunc { nArgs :: Int, label :: MLispLabel, toReg :: Int } |
    -- Halt
    Halt |
    -- Label (Intermediate only)
    Label { label :: MLispLabel }
    deriving (Show, Eq)

-- Function transform context
data MLispFuncCtx = MLispFuncCtx {
    -- Register stack top
    regStackTop :: Int,
    -- Function code
    funcCode :: Vector MLispInst,
    -- Function ID
    funcId :: Int,
    -- Next available local label ID
    nextLocalId :: Int
} deriving (Show, Eq)

-- AST transform context
data MLispTransCtx = MLispTransCtx {
    -- Current function context
    funcCtx :: MLispFuncCtx,
    -- Function context stack
    funcStack :: [MLispFuncCtx],
    -- Label-offset mapping
    labelOffsetMapping :: Map MLispLabel Int,
    -- Code
    code :: Vector MLispInst,
    -- Code length
    codeLen :: Int,
    -- Next available function label ID
    nextFuncId :: Int
} deriving (Show, Eq)

-- MLisp compile error
type MLispCompileError = String

-- AST transform operation
type MLispTransOp = ExceptT MLispCompileError (State MLispTransCtx)
