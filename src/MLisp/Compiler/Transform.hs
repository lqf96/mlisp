module MLisp.Compiler.Transform where

import Data.Ratio (numerator, denominator)
import Data.Map (Map, (!))

import MLisp.Util (setter)
import MLisp.Parser.AST (Statement(..), Expression(..))
import MLisp.Compiler.Types (MLispTransCtx(..), MLispFuncCtx(..), MLispInst(..), MLispTransOp, MLispLabel)
import MLisp.Compiler.Context (useRegs, popRegs, appendInst, makeLabel, getNextFuncLabel, saveFuncStack, popFuncStack, addToFuncCtx, getCtx)

-- [ AST Transform Phase ]
-- Transform statement
transformStatement :: Statement -> MLispTransOp ()
transformStatement stmt = case stmt of
    -- Skip
    (Skip) -> return ()
    -- If statement
    (IfStm { iCond = condExpr, trueStm = trueStmt, falseStm = falseStmt }) -> do
        -- Make labels
        trueLabel <- makeLabel
        completeLabel <- makeLabel
        -- Generate condition judgement code
        reg <- transformExpression condExpr
        appendInst Icj { condReg = reg, label = trueLabel }
        popRegs 1
        -- Generate false statements code
        transformStatement falseStmt
        appendInst Ijmp { label = completeLabel }
        -- Generate true statements code
        appendInst Label { label = trueLabel }
        transformStatement trueStmt
        appendInst Label { label = completeLabel }
    -- While statement
    (WhileStm { wCond = condExpr, stm = loopStmt }) -> do
        -- Make labels
        condCheckLabel <- makeLabel
        loopBeginLabel <- makeLabel
        loopEndLabel <- makeLabel
        -- Generate condition judgement code
        appendInst Label { label = condCheckLabel }
        reg <- transformExpression condExpr
        appendInst Icj { condReg = reg, label = loopBeginLabel }
        appendInst Ijmp { label = loopEndLabel }
        popRegs 1
        -- Generate loop body code
        appendInst Label { label = loopBeginLabel }
        transformStatement loopStmt
        appendInst Ijmp { label = condCheckLabel }
        appendInst Label { label = loopEndLabel }
    -- Return statement
    (ReturnStm { value = retExpr }) -> do
        reg <- transformExpression retExpr
        -- Generate "Ret" code
        appendInst Ret { retValReg = reg }
        -- Drop result register
        popRegs 1
    -- Set variable
    (SetVar { var = varId, value = valueExpr }) -> do
        reg1 <- useRegs 1
        -- Transform value expression
        reg2 <- transformExpression valueExpr
        -- Generate "Rmmov" code
        appendInst Irmovs { strVal = varId, toReg = reg1 }
        appendInst Rmmov { fromReg = reg2, toVarReg = reg1 }
        -- Release registers
        popRegs 2
    -- Make vector (Function "make-vector" + "set!")
    (MakeVec { var = varId, len = lenExpr }) -> do
        reg1 <- useRegs 1
        -- Transform "make-vector" function call
        let funcCallExpr = FunctionCall (VariableValue "make-vector") [lenExpr]
        reg2 <- transformExpression funcCallExpr
        -- Insert "rmmov" instruction
        appendInst Irmovs { strVal = varId, toReg = reg1 }
        appendInst Rmmov { fromReg = reg2, toVarReg = reg1 }
        -- Release registers
        popRegs 2
    -- Set vector element (Function "vector-set")
    (VecSet { var = varId, index = indexExpr, value = valueExpr }) -> do
        let vectorSetArgs = [StringLiteral varId, indexExpr, valueExpr]
        let funcCallExpr = FunctionCall (VariableValue "vector-set") vectorSetArgs
        -- Transform "vector-set" function call
        transformExpression funcCallExpr
        -- Drop result register
        popRegs 1
    -- Function definition
    (FunctionDef { name = nameId, argList = argIds, statements = stmts }) -> do
        -- [ Outer Function ]
        -- Make label and reserve registers
        funcLabel <- getNextFuncLabel
        funcReg <- useRegs 1
        funcNameReg <- useRegs 1
        -- Generate make function code
        appendInst Imkfunc { nArgs = length argIds, label = funcLabel, toReg = funcReg }
        appendInst Irmovs { strVal = nameId, toReg = funcNameReg }
        appendInst Rmmov { fromReg = funcReg, toVarReg = funcNameReg }
        -- Pop registers
        popRegs 2
        -- Save function context to stack
        saveFuncStack
        -- [ Inner Function ]
        appendInst Label { label = funcLabel }
        useRegs (length argIds + 1)
        -- Save function reference
        argNameReg <- useRegs 1
        appendInst Irmovs { strVal = nameId, toReg = argNameReg }
        appendInst Rmmov { fromReg = 0, toVarReg = argNameReg }
        -- Save arguments
        mapM_ (\(argReg, argId) -> do
            appendInst Irmovs { strVal = argId, toReg = argNameReg }
            appendInst Rmmov { fromReg = argReg, toVarReg = argNameReg }
            ) $ zip [1..] argIds
        -- Transform function statements
        transformStatement stmts
        -- Add a return statement as fallback
        fallbackRetReg <- useRegs 1
        appendInst Irmovn { toReg = fallbackRetReg }
        appendInst Ret { retValReg = fallbackRetReg }
        -- Pop function context from stack
        popFuncStack
    -- Expression
    (StmExpr expr) -> do
        transformExpression expr
        -- Drop result register
        popRegs 1
    -- Statement list
    (StmList stmts) -> mapM_ transformStatement stmts

-- Transform expression
transformExpression :: Expression -> MLispTransOp Int
transformExpression expr = case expr of
    -- Number literal
    (NumberLiteral numLit) -> do
        reg <- useRegs 1
        let num1 = numerator numLit
        let num2 = denominator numLit
        -- Use "irmovi" for integer operations
        if num2 == 1
            then appendInst Irmovi { intVal = num1, toReg = reg }
            else appendInst Irmovr { intVal1 = num1, intVal2 = num2, toReg = reg }
        return reg
    -- Character literal
    (CharLiteral charLit) -> do
        reg <- useRegs 1
        appendInst Irmovc { charVal = charLit, toReg = reg }
        return reg
    -- String literal
    (StringLiteral strLit) -> do
        reg <- useRegs 1
        appendInst Irmovs { strVal = strLit, toReg = reg }
        return reg
    -- Boolean literal
    (BoolLiteral boolLit) -> do
        reg <- useRegs 1
        appendInst Irmovb { boolVal = boolLit, toReg = reg }
        return reg
    -- Null literal
    (NilLiteral) -> do
        reg <- useRegs 1
        appendInst Irmovn { toReg = reg }
        return reg
    -- Variable value
    (VariableValue varId) -> do
        reg <- useRegs 1
        appendInst Irmovs { strVal = varId, toReg = reg }
        appendInst Mrmov { fromVarReg = reg, toReg = reg }
        return reg
    -- Function call
    (FunctionCall funcExpr argExprs) -> do
        reg <- transformExpression funcExpr
        -- Generate code for each register
        mapM_ transformExpression argExprs
        -- Insert call instructions
        appendInst Call { nArgs = length argExprs, baseReg = reg }
        -- Release registers and return current register
        popRegs $ length argExprs
        return reg
    -- Lambda function definition
    (LambdaDef varId funcExpr) -> do
        lambdaLabel <- getNextFuncLabel
        funcReg <- useRegs 1
        -- [ Outer Function ]
        appendInst Imkfunc { nArgs = 1, label = lambdaLabel, toReg = funcReg }
        -- Save function context to stack
        saveFuncStack
        -- [ Inner Function ]
        appendInst Label { label = lambdaLabel }
        useRegs 2
        -- Save lambda argument
        argReg <- useRegs 1
        appendInst Irmovs { strVal = varId, toReg = argReg }
        appendInst Rmmov { fromReg = 1, toVarReg = argReg }
        popRegs 1
        -- Wrap expression into return statement and translate
        transformStatement ReturnStm { value = funcExpr }
        -- Pop function context from stack
        popFuncStack
        -- Return function register
        return funcReg

-- AST transform phase
astTransformPhase :: Statement -> Bool -> MLispTransOp ()
astTransformPhase stmts runMain = do
    -- Append global code
    appendInst Label { label = (0, 0) }
    -- Transform statements
    transformStatement stmts
    -- Run main function
    if runMain
        -- (if
        then transformStatement IfStm {
            -- (has-var "main")
            iCond = (FunctionCall (VariableValue "has-var") [StringLiteral "main"]),
            -- (main)
            trueStm = StmExpr (FunctionCall (VariableValue "main") []),
            -- skip)
            falseStm = Skip
        }
        else return ()
    -- Append halt instruction as fallback
    appendInst Halt
    -- Add global code to context code
    ctx <- getCtx
    mapM_ addToFuncCtx $ funcCode $ funcCtx ctx

-- [ Resolve Label Phase ]
-- Resolve label with position
resolveLabelWithPos :: Map MLispLabel Int -> MLispInst -> MLispInst
resolveLabelWithPos mapping inst = case inst of
    -- Jump
    (Ijmp { label = l }) -> Jmp { addr = mapping ! l }
    -- Conditional jump
    (Icj { condReg = reg, label = l }) -> Cj { condReg = reg, addr = mapping ! l }
    -- Make function
    (Imkfunc { nArgs = nFuncArgs, label = l, toReg = reg }) ->
        Mkfunc { nArgs = nFuncArgs, addr = mapping ! l, toReg = reg }
    -- Do not change other labels
    _ -> inst

-- Resolve label phase
resolveLabelPhase :: MLispTransOp ()
resolveLabelPhase = setter (\ctx ->
    let instTransformer = resolveLabelWithPos $ labelOffsetMapping ctx
        newCode = fmap instTransformer $ code ctx
    in ctx { code = newCode })
