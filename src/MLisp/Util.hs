module MLisp.Util where

import Data.Word (Word8)
import Data.Bits (Bits, (.&.), shiftR, shiftL)
import Data.Map (Map, member, (!))
import Control.Monad.Trans (lift)
import Control.Monad.State (StateT, get, modify, put)
import Control.Monad.Except (ExceptT, throwError)

-- Shorthand for undefined (Used as type proxy)
u :: a
u = undefined

-- Find value at key, or return Nothing if key does not exist in map
(!?) :: (Ord k) => Map k a -> k -> Maybe a
m !? key = if member key m then Just (m ! key) else Nothing

-- Throw error if condition check failed
guard :: (Monad m) => Bool -> e -> ExceptT e m ()
guard cond err = if cond then return () else throwError err

-- Get current state
getState :: (Monad m) => ExceptT e (StateT s m) s
getState = lift $ get

-- Define a property getter
getter :: (Monad m) => (s -> a) -> ExceptT e (StateT s m) a
getter f = lift $ fmap f get

-- Set current state
setState :: (Monad m) => s -> ExceptT e (StateT s m) ()
setState state = lift $ put state

-- Define a property setter
setter :: (Monad m) => (s -> s) -> ExceptT e (StateT s m) ()
setter f = lift $ modify f

-- Build integer
buildInt :: (Num a, Bits a) => Word8 -> a -> a
buildInt nextByte current = shiftL current 8 + fromIntegral nextByte

-- Convert integer to variable length form
makeIntV :: Integer -> [Int]
makeIntV num = intVLen num : helper num
    where helper 0 = []
          helper num = fromIntegral (num .&. 0xff) : helper (shiftR num 8)

-- Convert integer to 4-bytes list form
makeInt32 :: Int -> [Int]
makeInt32 num = [byteAt 0, byteAt 1, byteAt 2, byteAt 3]
    where byteAt n = shiftR num (n * 8) .&. 0xff

-- Length of variable-length integer
intVLen :: Integer -> Int
intVLen 0 = 0
intVLen num = intVLen (shiftR num 8) + 1
