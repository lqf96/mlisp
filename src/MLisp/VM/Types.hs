module MLisp.VM.Types where

import Data.Dynamic (Dynamic)
import Data.Map (Map)
import Data.ByteString (ByteString)
import Data.Vector (Vector)
import Data.Ratio as Ratio (numerator, denominator, (%))
import Control.Monad.State (StateT)
import Control.Monad.Except (ExceptT)
import Numeric (showHex)

-- [ Types & Aliases ]
-- MLisp object
data MLispObj = MLispObj {
    -- Type name
    typeName :: String,
    -- Value
    value :: Dynamic
} deriving (Show)

-- MLisp variable scope
type MLispScope = Map String MLispObj

-- Stack frame type
data MLispStackFrame = MLispStackFrame {
    -- Local variables
    savedLocals :: Maybe MLispScope,
    -- Registers
    savedRegs :: Vector MLispObj,
    -- Result register
    resultReg :: Int,
    -- Counter
    savedCounter :: Int
} deriving (Show)

-- Virtual machine type
data MLispVM = MLispVM {
    -- Registers
    registers :: Vector MLispObj,
    -- Global variables
    globals :: MLispScope,
    -- Local variables
    locals :: Maybe MLispScope,
    -- Program data
    program :: ByteString,
    -- Program counter
    counter :: Int,
    -- Call stack
    callStack :: [MLispStackFrame],
    -- Write output function
    outputWriter :: String -> IO ()
}

-- MLisp number
data MLispNum = Number Rational | Inf | NegInf | NaN
    deriving (Eq)

-- MLisp pair
data MLispPair = MLispPair {
    -- Left value
    left :: MLispObj,
    -- Right value
    right :: MLispObj
} deriving (Show)

-- MLisp function implementation
data MLispFuncImpl =
    -- MLisp function
    MLispImpl { offset :: Int } |
    -- Native (HFFI) function
    NativeImpl { func :: Int -> MLispOp () }

-- MLisp function
data MLispFunc = MLispFunc {
    -- Captured scope
    scope :: MLispScope,
    -- Arguments amount
    nArgs :: Int,
    -- Implementation
    impl :: MLispFuncImpl
} deriving (Show)

-- MLisp runtime error
data MLispRuntimeError =
    -- Unknown opcode
    UnknownOpcode { opcode :: Int } |
    -- No local scope
    NoLocalScope |
    -- Program counter out of range
    CounterOutOfRange |
    -- Empty call stack
    EmptyCallStack |
    -- Variable not found
    VariableNotFound { varName :: String } |
    -- Call arguments mismatch
    CallArgsMismatch { expectNArgs :: Int, actualNArgs :: Int } |
    -- Type mismatch
    TypeMismatch { expectType :: String, actualType :: String } |
    -- Integer required
    IntegerRequired |
    -- HFFI error
    HFFIError { message :: String }
    deriving (Show)

-- Operation
type MLispOp = ExceptT MLispRuntimeError (StateT MLispVM IO)

-- [ Implementation ]
-- "Show" MLispFuncImpl
instance Show MLispFuncImpl where
    -- MLisp function
    show (MLispImpl { offset = addr }) = "<MLisp Function at 0x" ++ showHex addr ">"
    -- Native function
    show (NativeImpl {}) = "<Native Function>"

-- "Show" MLispNum
instance Show MLispNum where
    -- NaN
    show NaN = "NaN"
    -- Infinity
    show Inf = "Infinity"
    -- Negative infinity
    show NegInf = "-Infinity"
    -- Number
    show (Number num) = show num

-- "Ord" MLispNum
instance Ord MLispNum where
    -- [ Not Bigger Than ]
    -- NaN
    NaN <= _ = False
    _ <= NaN = False
    -- Infinity
    _ <= Inf = True
    Inf <= _ = False
    -- Negative infinity
    _ <= NegInf = True
    NegInf <= _ = False
    -- Number
    (Number x) <= (Number y) = x < y

-- "Num" MLispNum
instance Num MLispNum where
    -- [ Add ]
    -- NaN
    NaN + _ = NaN
    _ + NaN = NaN
    -- Infinity
    Inf + NegInf = NaN
    Inf + _ = Inf
    -- Negative infinity
    NegInf + Inf = NaN
    NegInf + _ = NegInf
    -- Number
    (Number x) + (Number y) = Number (x + y)
    -- [ Minus ]
    -- NaN
    NaN - _ = NaN
    _ - NaN = NaN
    -- Infinity
    Inf - Inf = NaN
    Inf - _ = Inf
    -- Negative infinity
    NegInf - NegInf = NaN
    NegInf - _ = NegInf
    -- Number
    (Number x) - (Number y) = Number (x - y)
    -- [ Multiple ]
    -- NaN
    NaN * _ = NaN
    _ * NaN = NaN
    -- Infinity
    Inf * Inf = Inf
    Inf * NegInf = NegInf
    Inf * (Number 0) = NaN
    Inf * (Number x) = if x > 0 then Inf else NegInf
    -- Negative infinity
    NegInf * Inf = NegInf
    NegInf * NegInf = Inf
    NegInf * (Number 0) = NaN
    NegInf * (Number x) = if x > 0 then NegInf else Inf
    -- Number
    (Number x) * (Number y) = Number (x * y)
    -- [ Absolute Value ]
    abs NaN = NaN
    abs Inf = Inf
    abs NegInf = Inf
    abs (Number num) = Number (abs num)
    -- [ Sign num ]
    signum NaN = NaN
    signum Inf = Number 1
    signum NegInf = Number (-1)
    signum (Number num) = Number (signum num)
    -- [ Negate ]
    negate NaN = NaN
    negate Inf = NegInf
    negate NegInf = Inf
    negate (Number num) = Number (negate num)
    -- [ From Integer ]
    fromInteger num = Number (fromInteger num)

-- "Fractional" MLispNum
instance Fractional MLispNum where
    -- [ Division ]
    -- NaN
    NaN / _ = NaN
    _ / NaN = NaN
    -- Infinity
    Inf / Inf = NaN
    Inf / NegInf = NaN
    Inf / (Number _) = Inf
    -- Negative infinity
    NegInf / Inf = NaN
    NegInf / NegInf = NaN
    NegInf / (Number _) = NegInf
    -- Number
    (Number _) / Inf = 0
    (Number _) / NegInf = 0
    (Number 0) / (Number 0) = NaN
    (Number x) / (Number 0) = if x > 0 then Inf else NegInf
    (Number x) / (Number y) = Number (x / y)
    -- [ From Rational ]
    fromRational x = Number x

-- [ Function ]
-- Construct a MLispNum
(%) :: Integer -> Integer -> MLispNum
-- NaN
0 % 0 = NaN
-- (-)Infinity
x % 0 = if x > 0 then Inf else NegInf
-- Number
x % y = Number (x Ratio.% y)

-- Numerator
numerator :: MLispNum -> Integer
-- NaN
numerator NaN = 0
-- Infinity
numerator Inf = 1
-- Negative infinity
numerator NegInf = -1
-- Number
numerator (Number num) = Ratio.numerator num

-- Denominator
denominator :: MLispNum -> Integer
-- NaN & Infinity
denominator NaN = 0
denominator Inf = 0
denominator NegInf = 0
-- Number
denominator (Number num) = Ratio.denominator num
