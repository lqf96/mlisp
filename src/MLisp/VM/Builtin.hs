module MLisp.VM.Builtin where

import Data.Map (union, fromList)
import Data.Vector as Vector (Vector, replicate, (//), (!), toList)
import Data.List (intersperse)
import Control.Monad.Except (throwError)

import MLisp.Util (guard)
import MLisp.VM.Types (
    MLispPair(..),
    MLispRuntimeError(..),
    MLispObj(..),
    MLispFunc(..),
    MLispNum,
    MLispOp,
    MLispScope)
import MLisp.VM.VM (getReg, getVar, setReg, setVar, runIO, getOutputWriter, hasVar)
import MLisp.VM.Object (
    makeObj,
    makePair,
    makeFunc,
    makeNativeFunc,
    unwrapObj,
    unwrapStr,
    unwrapInt,
    fromHaskellFunc1,
    fromHaskellFunc2
    )

-- [ Type Definitions ]
-- Haskell function types
type FuncNNN = MLispNum -> MLispNum -> MLispNum
type FuncNNB = MLispNum -> MLispNum -> Bool

-- [ Reflection Operations ]
-- Has variable
mLispHasVar :: MLispObj
mLispHasVar = makeNativeFunc 1 $ \baseReg -> do
    -- Arguments
    arg <- getReg (baseReg + 1)
    varName <- unwrapStr arg
    -- Return value
    result <- hasVar varName
    setReg baseReg $ makeObj result

-- [ Arithmetic Operations ]
-- Compare two objects
mLispCmp :: MLispObj
mLispCmp = makeNativeFunc 2 $ \baseReg -> do
    -- Arguments
    arg1 <- getReg (baseReg + 1)
    arg2 <- getReg (baseReg + 2)
    -- Type check
    guard (typeName arg1 == typeName arg2) TypeMismatch {
        expectType = typeName arg1,
        actualType = typeName arg2
    }
    -- Compare objects
    result <- case typeName arg1 of
        -- Null
        "Null" -> return True
        -- Character
        "Char" -> do
            value1 <- unwrapObj arg1 :: MLispOp Char
            value2 <- unwrapObj arg2 :: MLispOp Char
            return (value1 == value2)
        -- Boolean
        "Bool" -> do
            value1 <- unwrapObj arg1 :: MLispOp Bool
            value2 <- unwrapObj arg2 :: MLispOp Bool
            return (value1 == value2)
        -- Number
        "Number" -> do
            value1 <- unwrapObj arg1 :: MLispOp MLispNum
            value2 <- unwrapObj arg2 :: MLispOp MLispNum
            return (value1 == value2)
        -- Not supported
        _ -> throwError HFFIError { message = "Compare not supported for type \"" ++ typeName arg1 ++ "\"." }
    -- Return constructed pair
    setReg baseReg $ makeObj result

-- [ Pair Operations ]
-- Construct a pair
mLispCons :: MLispObj
mLispCons = makeNativeFunc 2 $ \baseReg -> do
    -- Arguments
    arg1 <- getReg (baseReg + 1)
    arg2 <- getReg (baseReg + 2)
    -- Return constructed pair
    setReg baseReg $ makePair arg1 arg2

-- Get left
mLispCar :: MLispObj
mLispCar = makeNativeFunc 1 $ \baseReg -> do
    -- Arguments
    argObj <- getReg (baseReg + 1)
    arg <- unwrapObj argObj :: MLispOp MLispPair
    -- Return left element
    setReg baseReg $ left arg

-- Get right
mLispCdr :: MLispObj
mLispCdr = makeNativeFunc 1 $ \baseReg -> do
    -- Arguments
    argObj <- getReg (baseReg + 1)
    arg <- unwrapObj argObj :: MLispOp MLispPair
    -- Return right element
    setReg baseReg $ right arg

-- [ Vector Operations ]
-- Get vector element
mLispVectorRef :: MLispObj
mLispVectorRef = makeNativeFunc 2 $ \baseReg -> do
    -- Arguments
    argObj1 <- getReg (baseReg + 1)
    argObj2 <- getReg (baseReg + 2)
    vector <- unwrapObj argObj1 :: MLispOp (Vector MLispObj)
    integerIndex <- unwrapInt argObj2
    -- Convert index to Int type
    let index = fromIntegral integerIndex :: Int
    -- Out-of-bound checking
    let vectorLen = length vector
    guard (index < length vector) HFFIError { message = "Array index out of bound." }
    -- Return array element
    setReg baseReg (vector ! index)

-- Make vector
mLispMakeVector :: MLispObj
mLispMakeVector = makeNativeFunc 1 $ \baseReg -> do
    -- Arguments
    argObj <- getReg (baseReg + 1)
    integerLen <- unwrapInt argObj
    -- Convert length to Int type
    let vectorLen = fromIntegral integerLen :: Int
    -- Return new vector
    let vector = Vector.replicate vectorLen $ makeObj ()
    setReg baseReg $ makeObj vector

-- Vector set
mLispVectorSet :: MLispObj
mLispVectorSet = makeNativeFunc 3 $ \baseReg -> do
    -- Parameters
    argObj1 <- getReg (baseReg + 1)
    argObj2 <- getReg (baseReg + 2)
    argObj3 <- getReg (baseReg + 3)
    vectorName <- unwrapStr argObj1
    integerIndex <- unwrapInt argObj2
    -- Convert index to Int type
    let index = fromIntegral integerIndex :: Int
    -- Get vector
    vectorObj <- getVar vectorName
    vector <- unwrapObj vectorObj :: MLispOp (Vector MLispObj)
    -- Out-of-bound checking
    let vectorLen = length vector
    guard (index < length vector) HFFIError { message = "Array index out of bound." }
    -- Set given argument
    let newVector = vector // [(index, argObj3)]
    setVar vectorName $ makeObj newVector
    -- Return nothing
    setReg baseReg $ makeObj ()

-- [ I/O Operations ]
-- Write output
writeOutput :: String -> MLispOp ()
writeOutput str = do
    writer <- getOutputWriter
    runIO $ writer str

-- Print object
printObj :: MLispObj -> MLispOp ()
printObj obj = case typeName obj of
    -- Null
    "Null" -> writeOutput "Nil"
    -- Character
    "Char" -> do
        value <- unwrapObj obj :: MLispOp Char
        writeOutput $ show value
    -- Boolean
    "Bool" -> do
        value <- unwrapObj obj :: MLispOp Bool
        writeOutput $ show value
    -- Vector
    "Array" -> do
        array <- unwrapObj obj :: MLispOp (Vector MLispObj)
        writeOutput "["
        -- Print each member
        let printObjActions = fmap printObj $ toList array
        sequence_ $ intersperse (writeOutput ", ") $ printObjActions
        writeOutput "]"
    -- Number
    "Number" -> do
        value <- unwrapObj obj :: MLispOp MLispNum
        writeOutput $ show value
    -- Pair
    "Pair" -> do
        pair <- unwrapObj obj :: MLispOp MLispPair
        -- Recursively print left and right
        writeOutput "<Pair Left = "
        printObj $ left pair
        writeOutput " Right = "
        printObj $ right pair
        writeOutput ">"
    -- Function
    "Function" -> do
        func <- unwrapObj obj :: MLispOp MLispFunc
        writeOutput $ show $ impl func
    -- Other types
    _ -> writeOutput "<Unknown Type>"

-- Print object
mLispPrintObj :: MLispObj
mLispPrintObj = makeNativeFunc 1 $ \baseReg -> do
    -- Parameters
    argObj <- getReg (baseReg + 1)
    -- Print object
    printObj argObj
    writeOutput "\n"
    -- Return nothing
    setReg baseReg $ makeObj ()

-- Print line
mLispPrintLn :: MLispObj
mLispPrintLn = makeNativeFunc 1 $ \baseReg -> do
    -- Parameters
    argObj <- getReg (baseReg + 1)
    str <- unwrapStr argObj
    -- Print object
    writeOutput str
    writeOutput "\n"
    -- Return nothing
    setReg baseReg $ makeObj ()

-- [ Global Scope Functions ]
-- Inject pure Haskell functions
injectHaskellFuncs :: MLispScope -> MLispScope
injectHaskellFuncs = union $ fromList [
        -- Add
        ("+", fromHaskellFunc2 ((+) :: FuncNNN)),
        -- Minus
        ("-", fromHaskellFunc2 ((-) :: FuncNNN)),
        -- Multiply
        ("*", fromHaskellFunc2 ((*) :: FuncNNN)),
        -- Divide
        ("/", fromHaskellFunc2 ((/) :: FuncNNN)),
        -- And
        ("and", fromHaskellFunc2 (&&)),
        -- Or
        ("or", fromHaskellFunc2 (||)),
        -- Not
        ("not", fromHaskellFunc1 not),
        -- Greater than
        (">", fromHaskellFunc2 ((>) :: FuncNNB)),
        -- Not smaller than
        (">=", fromHaskellFunc2 ((>=) :: FuncNNB)),
        -- Lower than
        ("<", fromHaskellFunc2 ((<) :: FuncNNB)),
        -- Not greater than
        ("<=", fromHaskellFunc2 ((<=) :: FuncNNB))
    ]

-- Inject HFFI functions
injectHffiFuncs :: MLispScope -> MLispScope
injectHffiFuncs = union $ fromList [
        -- Compare objects
        ("=", mLispCmp),
        -- Construct a pair
        ("cons", mLispCons),
        -- Get left
        ("car", mLispCar),
        -- Get right
        ("cdr", mLispCdr),
        -- Make vector
        ("make-vector", mLispMakeVector),
        -- Get vector element
        ("vector-ref", mLispVectorRef),
        -- Set vector element
        ("vector-set", mLispVectorSet),
        -- Print line
        ("print-line", mLispPrintLn),
        -- Print object
        ("print-obj", mLispPrintObj),
        -- Has variable
        ("has-var", mLispHasVar)
    ]
