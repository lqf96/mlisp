{-# LANGUAGE ScopedTypeVariables #-}

module MLisp.VM.Object where

import Data.Map (Map, fromList, empty)
import Data.Maybe (fromMaybe, fromJust)
import Data.Typeable (Typeable, typeOf)
import Data.Dynamic (toDyn, fromDynamic)

import MLisp.Util (guard, u, (!?))
import MLisp.VM.Types (MLispObj(..), MLispFuncImpl(..), MLispPair(..), MLispFunc(..), MLispRuntimeError(..), MLispNum, MLispScope, MLispOp, (%), numerator, denominator)
import MLisp.VM.VM (getReg, setReg)

-- [ Constants ]
-- Haskell type name mapping
typeNameMapping :: Map String String
typeNameMapping = fromList [
        -- "Bool" and "Char" remain the same, so not listed
        ("MLispNum", "Number"),
        ("MLispPair", "Pair"),
        ("()", "Null"),
        ("Vector MLispObj", "Array"),
        ("MLispFunc", "Function")
    ]

-- [ Functions ]
-- Get foreign type name
-- (Use mapping name first and fallback to Haskell type name)
foreignTypeName :: (Typeable a) => a -> String
foreignTypeName tp = fromMaybe rawTypeName (typeNameMapping !? rawTypeName)
    where rawTypeName = show $ typeOf tp

-- Make MLisp object
makeObj :: (Typeable a) => a -> MLispObj
makeObj value = MLispObj { typeName = foreignTypeName value, value = toDyn value }

-- Unwrap MLisp object
unwrapObj :: forall a. (Typeable a) => MLispObj -> MLispOp a
unwrapObj obj = do
    -- Type cast check
    let unwrapType = foreignTypeName (u :: a)
    let objType = typeName obj
    guard (unwrapType == objType) TypeMismatch {
        expectType = unwrapType,
        actualType = objType
    }
    -- Unwrap value
    let unwrapped = fromDynamic $ value obj :: Maybe a
    return $ fromJust unwrapped

-- Make MLisp integer
makeInt :: Integer -> MLispObj
makeInt value = makeObj (value % 1 :: MLispNum)

-- Unwrap MLisp integer
unwrapInt :: MLispObj -> MLispOp Integer
unwrapInt obj = do
    value <- unwrapObj obj :: MLispOp MLispNum
    -- Ensure denominator is 1 (Integer)
    guard (denominator value == 1) IntegerRequired
    -- Return numerator
    return $ numerator value

-- Make pair
makePair :: MLispObj -> MLispObj -> MLispObj
makePair leftVal rightVal = makeObj $ MLispPair { left = leftVal, right = rightVal }

-- Make MLisp string
makeStr :: String -> MLispObj
-- Empty string
makeStr "" = makeObj ()
-- Transform current character and remaining string to a pair
makeStr (char:remain) = makePair (makeObj char) $ makeStr remain

-- Unwrap MLisp string
unwrapStr :: MLispObj -> MLispOp String
unwrapStr obj
    -- "Null" corresponds with empty string
    | typeName obj == "Null" = return ""
    -- Pair
    | otherwise = do
        obj <- unwrapObj obj :: MLispOp MLispPair
        -- Extract next character and remaining string from pair
        leftChar <- unwrapObj $ left obj :: MLispOp Char
        rightStr <- unwrapStr $ right obj
        -- Assemble as a string
        return (leftChar : rightStr)

-- Make MLisp function
makeFunc :: MLispScope -> Int -> MLispFuncImpl -> MLispObj
makeFunc scope nArgs impl = makeObj $ MLispFunc { scope = scope, nArgs = nArgs, impl = impl }

-- Make MLisp native function
makeNativeFunc :: Int -> (Int -> MLispOp ()) -> MLispObj
makeNativeFunc nArgs func = makeFunc empty nArgs $ NativeImpl { func = func }

-- Make MLisp native function from pure Haskell function
fromHaskellFunc1 :: forall a b. (Typeable a, Typeable b) => (a -> b) -> MLispObj
fromHaskellFunc1 f = makeNativeFunc 1 $ \baseReg -> do
    -- Get arguments
    argObj <- getReg (baseReg + 1)
    arg <- unwrapObj argObj :: MLispOp a
    -- Call function and wrap result as MLisp value
    setReg baseReg $ makeObj $ f arg

fromHaskellFunc2 :: forall a b c. (Typeable a, Typeable b, Typeable c) => (a -> b -> c) -> MLispObj
fromHaskellFunc2 f = makeNativeFunc 2 $ \baseReg -> do
    -- Get arguments
    argObj1 <- getReg (baseReg + 1)
    argObj2 <- getReg (baseReg + 2)
    arg1 <- unwrapObj argObj1 :: MLispOp a
    arg2 <- unwrapObj argObj2 :: MLispOp b
    -- Call function and wrap result as MLisp value
    setReg baseReg $ makeObj $ f arg1 arg2
