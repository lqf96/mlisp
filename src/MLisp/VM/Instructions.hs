module MLisp.VM.Instructions where

import Data.Map (Map, fromList, empty)
import Data.ByteString as BS (ByteString, index, drop, take, foldr, length)
import Data.ByteString.Char8 (unpack)
import Data.Char (chr)
import Data.Word (Word8)
import Data.Bits (Bits, shiftL)
import Data.Foldable (toList)
import Data.Maybe (fromMaybe)
import Data.Vector ((!), (//))

import MLisp.Util (guard, buildInt)
import MLisp.VM.Types (MLispOp, MLispFunc(..), MLispFuncImpl(..), MLispRuntimeError(..), (%))
import MLisp.VM.Object (unwrapStr, unwrapObj, makeObj, makeStr, makeInt, makeFunc)
import MLisp.VM.VM (getReg, setReg, getLocals, getVar, setVar, getProgram, getCounter, setCounter, saveStackFrame, popStackFrame, getRegs, setRegs)

-- [ Constants ]
-- Halt opcode
haltOpcode :: Int
haltOpcode = 0x90

-- [ Functions ]
-- Fetch raw byte
fetchRawByte :: MLispOp Word8
fetchRawByte = do
    -- Get program and counter
    program <- getProgram
    counter <- getCounter
    -- Counter out-of-range check
    guard (counter < BS.length program) CounterOutOfRange
    -- Get byte
    let byte = index program counter
    -- Update counter
    setCounter (counter + 1)
    -- Return byte
    return byte

-- Fetch raw bytes
fetchRawBytes :: Int -> MLispOp ByteString
fetchRawBytes nBytes = do
    -- Get program and counter
    program <- getProgram
    counter <- getCounter
    -- Counter out-of-range check
    guard (counter + nBytes <= BS.length program) CounterOutOfRange
    -- Get bytes
    let bytes = BS.take nBytes $ BS.drop counter $ program
    -- Update counter
    setCounter (counter + nBytes)
    -- Return bytes
    return bytes

-- Fetch byte
fetchByte :: MLispOp Int
fetchByte = fmap fromIntegral fetchRawByte

-- Fetch character
fetchChar :: MLispOp Char
fetchChar = fmap chr fetchByte

-- Fetch integer
fetchInt :: MLispOp Integer
fetchInt = do
    -- Fetch integer length and number
    intLen <- fetchByte
    rawData <- fetchRawBytes intLen
    -- Make number
    return $ BS.foldr buildInt 0 rawData

-- Fetch string
fetchStr :: MLispOp String
fetchStr = do
    -- Fetch string length and string bytes
    strLen <- fetchAddr
    strData <- fetchRawBytes strLen
    -- Make string
    return $ unpack strData

-- Fetch address
fetchAddr :: MLispOp Int
fetchAddr = fmap (BS.foldr buildInt 0) $ fetchRawBytes 4

-- [ Instructions ]
-- Register-register movement
rrmov :: MLispOp ()
rrmov = do
    -- Registers
    reg0 <- fetchByte
    reg1 <- fetchByte
    -- Get first register value
    value <- getReg reg0
    -- Copy to second register
    setReg reg1 value
    -- Update counter
    counter <- getCounter
    setCounter (counter + 3)

-- Memory-register movement
mrmov :: MLispOp ()
mrmov = do
    -- Operands
    reg0 <- fetchByte
    reg1 <- fetchByte
    -- Get first register value
    wrappedName <- getReg reg0
    -- Get variable name
    name <- unwrapStr wrappedName
    -- Get variable value
    value <- getVar name
    -- Copy to second register
    setReg reg1 value

-- Register-memory movement
rmmov :: MLispOp ()
rmmov = do
    -- Operands
    reg0 <- fetchByte
    reg1 <- fetchByte
    -- Get first register value
    value <- getReg reg0
    -- Get second register value
    wrappedName <- getReg reg1
    -- Get variable name
    name <- unwrapStr wrappedName
    -- Set variable value
    setVar name value

-- Immediate-register movement
-- (Null)
irmovn :: MLispOp ()
irmovn = do
    -- Operands
    reg <- fetchByte
    -- Set register
    setReg reg $ makeObj ()

-- (String)
irmovs :: MLispOp ()
irmovs = do
    -- Operands
    value <- fetchStr
    reg <- fetchByte
    -- Set register
    setReg reg $ makeStr value

-- (Integer)
irmovi :: MLispOp ()
irmovi = do
    -- Operands
    value <- fetchInt
    reg <- fetchByte
    -- Set register
    setReg reg $ makeInt value

-- (Boolean)
irmovb :: MLispOp ()
irmovb = do
    -- Operands
    value <- fetchByte
    reg <- fetchByte
    -- Set register
    setReg reg $ makeObj (value /= 0)

-- (Character)
irmovc :: MLispOp ()
irmovc = do
    -- Operands
    value <- fetchChar
    reg <- fetchByte
    -- Set register
    setReg reg $ makeObj value

-- (Rational)
irmovr :: MLispOp ()
irmovr = do
    -- Operands
    num1 <- fetchInt
    num2 <- fetchInt
    reg <- fetchByte
    -- Set register
    setReg reg $ makeObj (num1 % num2)

-- Call function
call :: MLispOp ()
call = do
    -- Operands
    nCallArgs <- fetchByte
    reg <- fetchByte
    -- Fetch function
    wrappedFuncObj <- getReg reg
    funcObj <- unwrapObj wrappedFuncObj :: MLispOp MLispFunc
    -- Check amount of arguments
    guard (nCallArgs == nArgs funcObj) CallArgsMismatch {
        expectNArgs = nArgs funcObj,
        actualNArgs = nCallArgs
    }
    -- Call function
    case impl funcObj of
        -- MLisp implementation
        (MLispImpl { offset = funcEntry }) -> do
            -- Save stack frame
            saveStackFrame reg
            -- Move argument registers
            regs <- getRegs
            let makeUpdateReg i = (i, regs ! (reg + i))
            let updateRegs = fmap makeUpdateReg [0..nCallArgs]
            setRegs (regs // updateRegs)
            -- Jump to function entry
            setCounter funcEntry
        -- Native implementation; call HFFI function
        (NativeImpl { func = nativeFunc }) -> nativeFunc reg

-- Return from function
ret :: MLispOp ()
ret = do
    -- Operands
    returnReg <- fetchByte
    -- Get return value
    value <- getReg returnReg
    -- Pop stack frame
    resultReg <- popStackFrame
    -- Set result register
    setReg resultReg value

-- Jump to given location
jmp :: MLispOp ()
jmp = do
    -- Operands
    addr <- fetchAddr
    -- Set counter to given address
    setCounter addr

-- Conditional jump
cj :: MLispOp ()
cj = do
    -- Operandsb
    reg <- fetchByte
    addr <- fetchAddr
    -- Get boolean value
    boolObj <- getReg reg
    boolVal <- unwrapObj boolObj :: MLispOp Bool
    -- Set counter to given address if value is true
    if boolVal
        then setCounter addr
        else return ()

-- Make funcion
mkfunc :: MLispOp ()
mkfunc = do
    -- Operands
    nFuncArgs <- fetchByte
    funcEntry <- fetchAddr
    reg <- fetchByte
    -- Get local scope
    outerScope <- getLocals
    let captureScope = fromMaybe empty outerScope
    -- Create function
    let f = makeFunc captureScope nFuncArgs $ MLispImpl { offset = funcEntry }
    -- Set register
    setReg reg f

-- [ Opcode-Instruction Mapping ]
opcodeInstMapping :: Map Int (MLispOp ())
opcodeInstMapping = fromList [
        -- Move instructions
        (0x00, rrmov),
        (0x10, rmmov),
        (0x20, mrmov),
        -- Immediate operand move instructions
        (0x30, irmovn),
        (0x31, irmovi),
        (0x32, irmovc),
        (0x33, irmovs),
        (0x34, irmovb),
        (0x35, irmovr),
        -- Subroutine instructions
        (0x40, call),
        (0x50, ret),
        -- Jump instructions
        (0x60, jmp),
        (0x70, cj),
        -- High-order function instructions
        (0x80, mkfunc)
    ]
