# MLisp
The MLisp programming language, compiler and interpreter.

## Build / Install
### Unix-like
* Ensure `stack` and `strip` is installed, and the stack environment is initialized (You can run `stack setup` to initialize `stack` environment).
* Run `./install.sh` to install MLisp compiler and interpreter. To customize install location set environment variable `BIN_PATH`.
### Windows
See [report](Report.pdf) for how to build and install on Windows.

## CLI Usage
You should provide command and arguments when invoking `mlisp`. (e.g. `mlisp ki -i examples/factorial.mlisp`) To directly invoke each individual command, symlink or copy `mlisp` to name of the command. This is automatically done by running `./install.sh`. See [report](Report.pdf) for more information.
### Interpreter (ki)
```sh
ki [-h] [-repl] [-i <Input>] [-t <Print AST Input>] [-o <Output>]
```
### Compiler (kc)
```sh
kc [-h] [<Input>] [-o <Output>]
```

### REPL Mode
The following commands are supported in REPL mode:
* `:help`: Show this help.
* `[:i] [<program>]`: Execute MLisp program. If source code is not given, run last program entered.
* `[:t] <program>`: Show AST of given MLisp program. If source code is not given, show AST of last program entered.
* `:| <program> :|`: Similar to `:i`, but supported multi-line input.
* `:q`: Exit from interpreter.

## License
[BSD 3-Clause License](LICENSE)
