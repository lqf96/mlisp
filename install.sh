#! /bin/sh
set -e

# Default binary path
if [ -z "${BIN_PATH}" ]; then
    BIN_PATH="`stack path --local-bin`"
fi

# Build and install executable first
mkdir -p "${BIN_PATH}"
stack install --local-bin-path "${BIN_PATH}"
strip "${BIN_PATH}/mlisp"
# Then symlink executable
cd "${BIN_PATH}"
ln -sf mlisp kc
ln -sf mlisp ki
