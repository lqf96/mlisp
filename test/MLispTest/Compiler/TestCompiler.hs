{-# LANGUAGE OverloadedStrings #-}

module MLispTest.Compiler.TestCompiler where

import Test.QuickCheck
import MLispTest.TestCase

import MLisp.Compiler
import MLisp.Parser
import Control.Monad.Except
import Control.Monad.State
import Data.ByteString (ByteString)

testTranslate :: Statement -> MLispTransOp (ByteString, Int)
testTranslate ast  = do
    transformAST ast False
    byteCode <- genByteCode
    entryPoint <- getEntryPoint
    return (byteCode, entryPoint)

testCompilerEntry :: IO()
testCompilerEntry = do
    putStrLn "----------SUBSECTION: BYTECODE----------"
    quickCheck testEncodeInst1
    quickCheck testEncodeInst2
    quickCheck testEncodeInst3 
    quickCheck testEncodeInst4 
    quickCheck testEncodeInst5 
    quickCheck testEncodeInst6
    quickCheck testEncodeInst7
    quickCheck testEncodeInst8
    quickCheck testEncodeInst9
    quickCheck testEncodeInst10
    quickCheck testEncodeInst11
    quickCheck testEncodeInst12
    quickCheck testEncodeInst13
    quickCheck testEncodeInst14
    quickCheck testEncodeInst15
    putStrLn "-----encodeInst finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testGenByteCode1
    quickCheck testGenByteCode2
    quickCheck testGenByteCode3
    quickCheck testGenByteCode4
    putStrLn "-----genByteCode finished-----"
    putStrLn "----------SUBSECTION: BYTECODE----------"
    putStrLn ""
    putStrLn ""
    putStrLn "----------SUBSECTION: CONTEXT----------"
    quickCheck testInstLen1
    quickCheck testInstLen2
    quickCheck testInstLen3
    quickCheck testInstLen4
    quickCheck testInstLen5
    quickCheck testInstLen6
    quickCheck testInstLen7
    quickCheck testInstLen8
    quickCheck testInstLen9
    quickCheck testInstLen10
    quickCheck testInstLen11
    quickCheck testInstLen12
    quickCheck testInstLen13
    quickCheck testInstLen14
    quickCheck testInstLen15
    quickCheck testInstLen16
    quickCheck testInstLen17
    quickCheck testInstLen18
    quickCheck testInstLen19
    putStrLn "----------SUBSECTION: CONTEXT----------"
    putStrLn ""
    putStrLn ""
    putStrLn "----------SUBSECTION: TRANSFORM----------"
    quickCheck testTransformStatement1
    quickCheck testTransformStatement2
    quickCheck testTransformStatement3
    quickCheck testTransformStatement4
    quickCheck testTransformStatement5
    quickCheck testTransformStatement6
    quickCheck testTransformStatement7
    quickCheck testTransformStatement8
    quickCheck testTransformStatement9
    quickCheck testTransformStatement10
    putStrLn "-----transformStatement finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testTransformExpression1
    quickCheck testTransformExpression2
    quickCheck testTransformExpression3
    quickCheck testTransformExpression4
    quickCheck testTransformExpression5
    quickCheck testTransformExpression6
    quickCheck testTransformExpression7
    quickCheck testTransformExpression8
    putStrLn "-----transformExpression finished-----"
    putStrLn "----------SUBSECTION: TRANSFORM----------"

----------SUBSECTION: BYTECODE----------
testEncodeInst1 = (encodeInst exampleRrmov == [0x0, 0, 1])
testEncodeInst2 = (encodeInst exampleRmmov == [0x10, 0, 2])
testEncodeInst3 = (encodeInst exampleMrmov == [0x20, 2, 1])
testEncodeInst4 = (encodeInst exampleIrmovn == [0x30, 1])
testEncodeInst5 = (encodeInst exampleIrmovi == [0x31, 1, 233, 1])
testEncodeInst6 = (encodeInst exampleIrmovc == [0x32, 97, 1])
testEncodeInst7 = (encodeInst exampleIrmovs == [0x33, 14, 0, 0, 0, 101,120,97,109,112,108,101,32,115,116,114,105,110,103, 1])
testEncodeInst8 = (encodeInst exampleIrmovb == [0x34, 1, 1])
testEncodeInst9 = (encodeInst exampleIrmovr == [0x35, 2, 0, 1, 1, 2, 1])
testEncodeInst10 = (encodeInst exampleCall == [0x40, 5, 1])
testEncodeInst11 = (encodeInst exampleRet == [0x50, 6])
testEncodeInst12 = (encodeInst exampleJmp == [0x60, 174, 8, 0, 0])
testEncodeInst13 = (encodeInst exampleCj == [0x70, 3, 233, 0, 0, 0])
testEncodeInst14 = (encodeInst exampleMkfunc == [0x80, 3, 12, 48, 0, 0, 3])
testEncodeInst15 = (encodeInst exampleHalt == [0x90])

exampleStatement1 = Skip
exampleStatement2 = (IfStm {iCond = NumberLiteral 1, trueStm = SetVar {var = "temp", value = BoolLiteral True},
 falseStm = SetVar {var = "temp", value = BoolLiteral False}})
exampleStatement3 = StmList [Skip, (IfStm (BoolLiteral True) Skip Skip), (WhileStm (BoolLiteral True) Skip), (ReturnStm (NumberLiteral 1)), (SetVar "exampleVar" (NumberLiteral 1)), (MakeVec "exampleVec" (NumberLiteral 10)), (VecSet "exampleVec" (NumberLiteral 1) (NumberLiteral 1)), (FunctionDef "exampleFunc" ["arg1"] Skip)]
exampleMLispTransCtx1 = makeTransCtx
exampleMLispTransCtx2 = execTransOp (testTranslate exampleStatement1) makeTransCtx 
exampleMLispTransCtx3 = execTransOp (testTranslate exampleStatement2) makeTransCtx
exampleMLispTransCtx4 = execTransOp (testTranslate exampleStatement3) makeTransCtx
testGenByteCode1 = (evalTransOp genByteCode exampleMLispTransCtx1 == Right "")
testGenByteCode2 = (evalTransOp genByteCode exampleMLispTransCtx2 == Right "\144")
testGenByteCode3 = (evalTransOp genByteCode exampleMLispTransCtx3 == Right "1\SOH\SOH\NULp\NUL\US\NUL\NUL\NUL4\NUL\SOH3\EOT\NUL\NUL\NULtemp\NUL\DLE\SOH\NUL`/\NUL\NUL\NUL4\SOH\SOH3\EOT\NUL\NUL\NULtemp\NUL\DLE\SOH\NUL\144")
testGenByteCode4 = (evalTransOp genByteCode exampleMLispTransCtx4 == Right "3\v\NUL\NUL\NULexampleFunc\STX\DLE\NUL\STX3\EOT\NUL\NUL\NULarg1\STX\DLE\SOH\STX0\ETXP\ETX4\SOH\NULp\NUL3\NUL\NUL\NUL`3\NUL\NUL\NUL4\SOH\NULp\NULA\NUL\NUL\NUL`F\NUL\NUL\NUL`3\NUL\NUL\NUL1\SOH\SOH\NULP\NUL1\SOH\SOH\SOH3\n\NUL\NUL\NULexampleVar\NUL\DLE\SOH\NUL3\v\NUL\NUL\NULmake-vector\SOH \SOH\SOH1\SOH\n\STX@\SOH\SOH3\n\NUL\NUL\NULexampleVec\NUL\DLE\SOH\NUL3\n\NUL\NUL\NULvector-set\NUL \NUL\NUL3\n\NUL\NUL\NULexampleVec\SOH1\SOH\SOH\STX1\SOH\SOH\ETX@\ETX\NUL\128\SOH\NUL\NUL\NUL\NUL\NUL3\v\NUL\NUL\NULexampleFunc\SOH\DLE\NUL\SOH\144")
----------SUBSECTION: BYTECODE----------

----------SUBSECTION: CONTEXT----------
testInstLen1 = (instLen exampleRrmov == 3)
testInstLen2 = (instLen exampleRmmov == 3)
testInstLen3 = (instLen exampleMrmov == 3)
testInstLen4 = (instLen exampleIrmovn == 2)
testInstLen5 = (instLen exampleIrmovi == 4)
testInstLen6 = (instLen exampleIrmovb == 3)
testInstLen7 = (instLen exampleIrmovc == 3)
testInstLen8 = (instLen exampleIrmovs == 20)
testInstLen9 = (instLen exampleIrmovr == 7)
testInstLen10 = (instLen exampleCall == 3)
testInstLen11 = (instLen exampleRet == 2)
testInstLen12 = (instLen exampleJmp == 5)
testInstLen13 = (instLen exampleIjmp == 5)
testInstLen14 = (instLen exampleCj == 6)
testInstLen15 = (instLen exampleIcj == 6)
testInstLen16 = (instLen exampleMkfunc == 7)
testInstLen17 = (instLen exampleImkfunc == 7)
testInstLen18 = (instLen exampleHalt == 1)
testInstLen19 = (instLen exampleLabel == 0)
----------SUBSECTION: CONTEXT----------

----------SUBSECTION: TRANSFORM----------
testTransformStatement1 = (execTransOp (transformStatement Skip) makeTransCtx == expectedCtx1)
testTransformStatement2 = (execTransOp (transformStatement expectedIfStm) makeTransCtx == expectedCtx2)
testTransformStatement3 = (execTransOp (transformStatement expectedWhileStm1) makeTransCtx == expectedCtx3)
testTransformStatement4 = (execTransOp (transformStatement expectedReturnStm1) makeTransCtx == expectedCtx4)
testTransformStatement5 = (execTransOp (transformStatement expectedSetVarExpr1) makeTransCtx == expectedCtx5)
testTransformStatement6 = (execTransOp (transformStatement expectedMakeVecExpr) makeTransCtx == expectedCtx6)
testTransformStatement7 = (execTransOp (transformStatement expectedVecSetExpr1) makeTransCtx == expectedCtx7)
testTransformStatement8 = (execTransOp (transformStatement expectedFunctionDefStm1) makeTransCtx == expectedCtx8)
testTransformStatement9 = (execTransOp (transformStatement expectedStmExpr) makeTransCtx == expectedCtx9)
testTransformStatement10 = (execTransOp (transformStatement expectedStmList) makeTransCtx == expectedCtx10)
testTransformExpression1 = (execTransOp (transformExpression expectedNumberLiteralExpr) makeTransCtx == expectedCtx11)
testTransformExpression2 = (execTransOp (transformExpression expectedCharLiteralExpr) makeTransCtx == expectedCtx12)
testTransformExpression3 = (execTransOp (transformExpression expectedStringLiteralExpr) makeTransCtx == expectedCtx13)
testTransformExpression4 = (execTransOp (transformExpression expectedBoolLiteralExpr1) makeTransCtx == expectedCtx14)
testTransformExpression5 = (execTransOp (transformExpression expectedNilLiteralExpr) makeTransCtx == expectedCtx15)
testTransformExpression6 = (execTransOp (transformExpression expectedVariableValueExpr) makeTransCtx == expectedCtx16)
testTransformExpression7 = (execTransOp (transformExpression expectedFunctionCallExpr1) makeTransCtx == expectedCtx17)
testTransformExpression8 = (execTransOp (transformExpression expectedLambdaDefExpr) makeTransCtx == expectedCtx18)
----------SUBSECTION: TRANSFORM----------