module MLispTest.TestUtil where

import Test.QuickCheck
import MLisp.Util

testUtilEntry :: IO()
testUtilEntry = do
    quickCheck testBuildInt1
    quickCheck testBuildInt2
    quickCheck testBuildInt3
    putStrLn "buildInt finished"
    putStrLn ""
    putStrLn ""
    quickCheck testMakeIntV1
    quickCheck testMakeIntV2
    quickCheck testMakeIntV3
    quickCheck testMakeIntV4
    quickCheck testMakeIntV5
    quickCheck testMakeIntV6
    putStrLn "makeIntV finished"
    putStrLn ""
    putStrLn ""
    quickCheck testMakeIntThirtyTwo1
    quickCheck testMakeIntThirtyTwo2
    quickCheck testMakeIntThirtyTwo3
    quickCheck testMakeIntThirtyTwo4
    quickCheck testMakeIntThirtyTwo5
    putStrLn "makeInt32 finished"
    putStrLn ""
    putStrLn ""
    quickCheck testIntVLen1
    quickCheck testIntVLen2
    quickCheck testIntVLen3
    quickCheck testIntVLen4
    quickCheck testIntVLen5
    putStrLn "intVLen finished"
    
--buildInt
testBuildInt1 = (buildInt 0 (0::Integer) == 0)
testBuildInt2 = (buildInt 23 (3::Integer) == 791)
testBuildInt3 = (buildInt 255 (23::Integer) == 6143)

--makeIntV
testMakeIntV1 = (makeIntV 32 == [1,32])
testMakeIntV2 = (makeIntV 255 == [1,255])
testMakeIntV3 = (makeIntV 256 == [2,0,1])
testMakeIntV4 = (makeIntV 65535 == [2,255,255])
testMakeIntV5 = (makeIntV (256*256) == [3,0,0,1])
testMakeIntV6 = (makeIntV 0 == [0])

--makeInt32
testMakeIntThirtyTwo1 = (makeInt32 32 == [32,0,0,0])
testMakeIntThirtyTwo2 = (makeInt32 65535 == [255,255,0,0]) 
testMakeIntThirtyTwo3 = (makeInt32 233233 == [17,143,3,0]) 
testMakeIntThirtyTwo4 = (makeInt32 49583240589 == [141,53,100,139])
testMakeIntThirtyTwo5 = (makeInt32 0 == [0,0,0,0])

--intVLen
testIntVLen1 = (intVLen 0 == 0)
testIntVLen2 = (intVLen 255 == 1)
testIntVLen3 = (intVLen 256 == 2)
testIntVLen4 = (intVLen (255*255) == 2)
testIntVLen5 = (intVLen (256 * 256) == 3)