module MLispTest.TestCase where
    
import MLisp.Parser
import MLisp.Compiler
import Data.Vector(fromList)
import Data.Map(fromList)
import Control.Monad.Except
import Control.Monad.State
import Data.ByteString (ByteString)

---string case for TestProgram
skipStmString1 = "skip"
skipStmString2 = " skip"
ifStmString = "(if True (set! var1 True) (set! var1 False))"
whileStmString1 = "(while False (set! var 1))"
whileStmString2 = "(while False (set! var 2))"
returnStmString = "(define (returnFunc) (return (+ 1 2)))"
setVarStmString = "(set! tempVar (+ 1 2))"
makeVecStmString = "(make-vector exampleVec 10)"
vecSetStmString1 = "(make-vector exampleVec 10) (vector-set! exampleVec 2 (+ 1 2))"
vecSetStmString2 = "(make-vector exampleVec 10) (vector-set! exampleVec (+ 3 4) 4)"
stmListStmString = "skip (if True skip skip) (while True skip) (return 1) (set! exampleVar 1) (make-vector exampleVec 10) (vector-set! exampleVec 1 1) (define (exampleFunc arg1) skip)"
numberExprString = "1"
charExprString = "'a'"
boolExprString1 = "True"
boolExprString2 = "False"
nilExprString = "nil"
variableValueExprString = "(set! var 2) (print-obj var)"
functionCallString1 = "(+ 1 2)"
functionCallString2 = "(define (voidFunc) skip) (voidFunc)"
lambdaDefExprString = "((lambda arg (+ arg 1)) 2)"
functionDefStmString1 = "(define (exampleFunc arg1 arg2) (if True (return arg1) (return arg2)))"
functionDefStmString2 = "(define (voidFunc) skip)"
---




---ast case for TestParser
expectedStmList = (StmList [Skip, StmExpr (NumberLiteral 1), StmExpr (NumberLiteral 2)])
expectedSkip = Skip
expectedIfStm = (IfStm {iCond = NumberLiteral 1, trueStm = SetVar {var = "temp", value = BoolLiteral True},
 falseStm = SetVar {var = "temp", value = BoolLiteral False}})
expectedWhileStm1 = (WhileStm {wCond = BoolLiteral True, stm = StmExpr (FunctionCall (VariableValue "+") [NumberLiteral 1, NumberLiteral 2])})
expectedWhileStm2 = (WhileStm {wCond = BoolLiteral False, stm = StmExpr (FunctionCall (VariableValue "-") [NumberLiteral 10, NumberLiteral 9])})
expectedReturnStm1 = (ReturnStm {value = NumberLiteral 1})
expectedReturnStm2 = (ReturnStm {value = FunctionCall (VariableValue "+") [NumberLiteral 1, NumberLiteral 2]})
expectedSetVarExpr1 = (SetVar {var = "exampleVar", value = NumberLiteral 1})
expectedSetVarExpr2 = (SetVar {var = "exampleVar", value = BoolLiteral True})
expectedMakeVecExpr = (MakeVec {var = "exampleVec", len = NumberLiteral 10})
expectedVecSetExpr1 = (VecSet {var = "exampleVec", index = NumberLiteral 2, value = (FunctionCall (VariableValue "+") [NumberLiteral 1, NumberLiteral 2])})
expectedVecSetExpr2 = (VecSet {var = "exampleVec", index = (FunctionCall (VariableValue "+") [NumberLiteral 3, NumberLiteral 4]), value = NumberLiteral 4})
expectedStmExpr = (StmExpr (NumberLiteral 1))
expectedStatementListStm = [Skip, (IfStm (BoolLiteral True) Skip Skip), (WhileStm (BoolLiteral True) Skip), (ReturnStm (NumberLiteral 1)), (SetVar "exampleVar" (NumberLiteral 1)), (MakeVec "exampleVec" (NumberLiteral 10)), (VecSet "exampleVec" (NumberLiteral 1) (NumberLiteral 1)), (FunctionDef "exampleFunc" ["arg1"] Skip)]
expectedNumberLiteralExpr = (NumberLiteral 1)
expectedCharLiteralExpr = (CharLiteral 'a')
expectedStringLiteralExpr = (StringLiteral "foo")
expectedBoolLiteralExpr1 = (BoolLiteral True)
expectedBoolLiteralExpr2 = (BoolLiteral False)
expectedNilLiteralExpr = NilLiteral
expectedVariableValueExpr = (VariableValue "val")
expectedFunctionCallExpr1 = (FunctionCall (VariableValue "+") [NumberLiteral 1, NumberLiteral 2])
expectedFunctionCallExpr2 = (FunctionCall (VariableValue "voidFunc") [])
expectedLambdaDefExpr = (LambdaDef "arg" (NumberLiteral 1))
expectedFunctionDefStm1 = (FunctionDef {name = "example", argList = ["arg1", "arg2"], statements = Skip})
expectedFunctionDefStm2 = (FunctionDef {name = "voidFunc", argList = [], statements = Skip})
expectedIdentifier1 = "id1"
expectedIdentifier2 = "id2"
expectedIdentifier3 = "i"
expectedIdentifier4 = "(id1"
expectedInvalidChar1 = ' '
expectedInvalidChar2 = '('
expectedInvalidChar3 = ')'
expectedInvalidChar4 = ','

--example inst for TestCompiler/bytecode
exampleRrmov = Rrmov {fromReg = 0, toReg = 1}
exampleRmmov = Rmmov {fromReg = 0, toVarReg = 2}
exampleMrmov = Mrmov {fromVarReg = 2, toReg = 1}

exampleIrmovn = Irmovn {toReg = 1}
exampleIrmovi = Irmovi {intVal = 233, toReg = 1}
exampleIrmovb = Irmovb {boolVal = True, toReg = 1}
exampleIrmovc = Irmovc {charVal = 'a', toReg = 1}
exampleIrmovs = Irmovs {strVal = "example string", toReg = 1}
exampleIrmovr = Irmovr {intVal1 = 256, intVal2 = 2, toReg = 1}

exampleCall = Call {nArgs = 5, baseReg = 1}
exampleRet = Ret {retValReg = 6}
exampleJmp = Jmp {addr = 2222}
exampleIjmp = Ijmp {MLisp.Compiler.label = (2,3)}
exampleCj = Cj {condReg = 3, addr = 233}
exampleIcj = Icj {condReg = 3, MLisp.Compiler.label = (2,3)}

exampleMkfunc = Mkfunc {nArgs = 3, addr = 12300, toReg = 3}
exampleImkfunc = Imkfunc {nArgs = 4, MLisp.Compiler.label = (1,2), toReg = 4}
exampleHalt = Halt

exampleLabel = Label {MLisp.Compiler.label = (1, 2)}

--ctx case for TestCompiler/context
expectedCtx1 = makeTransCtx
expectedCtx2 = MLispTransCtx {funcCtx = MLispFuncCtx {regStackTop = 0, funcCode = Data.Vector.fromList [Irmovi {intVal = 1, toReg = 0},Icj {condReg = 0, MLisp.Compiler.label = (0,1)},Irmovb {boolVal = False, toReg = 1},Irmovs {strVal = "temp", toReg = 0},Rmmov {fromReg = 1, toVarReg = 0},Ijmp {MLisp.Compiler.label = (0,2)},Label {MLisp.Compiler.label = (0,1)},Irmovb {boolVal = True, toReg = 1},Irmovs {strVal = "temp", toReg = 0},Rmmov {fromReg = 1, toVarReg = 0},Label {MLisp.Compiler.label = (0,2)}], funcId = 0, nextLocalId = 3}, funcStack = [], labelOffsetMapping = Data.Map.fromList [], code = Data.Vector.fromList [], codeLen = 0, nextFuncId = 1}
expectedCtx3 = MLispTransCtx {funcCtx = MLispFuncCtx {regStackTop = 0, funcCode = Data.Vector.fromList [Label {MLisp.Compiler.label = (0,1)},Irmovb {boolVal = True, toReg = 0},Icj {condReg = 0, MLisp.Compiler.label = (0,2)},Ijmp {MLisp.Compiler.label = (0,3)},Label {MLisp.Compiler.label = (0,2)},Irmovs {strVal = "+", toReg = 0},Mrmov {fromVarReg = 0, toReg = 0},Irmovi {intVal = 1, toReg = 1},Irmovi {intVal = 2, toReg = 2},Call {nArgs = 2, baseReg = 0},Ijmp {MLisp.Compiler.label = (0,1)},Label {MLisp.Compiler.label = (0,3)}], funcId = 0, nextLocalId = 4}, funcStack = [], labelOffsetMapping = Data.Map.fromList [], code = Data.Vector.fromList [], codeLen = 0, nextFuncId = 1}
expectedCtx4 = MLispTransCtx {funcCtx = MLispFuncCtx {regStackTop = 0, funcCode = Data.Vector.fromList [Irmovi {intVal = 1, toReg = 0},Ret {retValReg = 0}], funcId = 0, nextLocalId = 1}, funcStack = [], labelOffsetMapping = Data.Map.fromList [], code = Data.Vector.fromList [], codeLen = 0, nextFuncId = 1}
expectedCtx5 = MLispTransCtx {funcCtx = MLispFuncCtx {regStackTop = 0, funcCode = Data.Vector.fromList [Irmovi {intVal = 1, toReg = 1},Irmovs {strVal = "exampleVar", toReg = 0},Rmmov {fromReg = 1, toVarReg = 0}], funcId = 0, nextLocalId = 1}, funcStack = [], labelOffsetMapping = Data.Map.fromList [], code = Data.Vector.fromList [], codeLen = 0, nextFuncId = 1}
expectedCtx6 = MLispTransCtx {funcCtx = MLispFuncCtx {regStackTop = 0, funcCode = Data.Vector.fromList [Irmovs {strVal = "make-vector", toReg = 1},Mrmov {fromVarReg = 1, toReg = 1},Irmovi {intVal = 10, toReg = 2},Call {nArgs = 1, baseReg = 1},Irmovs {strVal = "exampleVec", toReg = 0},Rmmov {fromReg = 1, toVarReg = 0}], funcId = 0, nextLocalId = 1}, funcStack = [], labelOffsetMapping = Data.Map.fromList [], code = Data.Vector.fromList [], codeLen = 0, nextFuncId = 1}
expectedCtx7 = MLispTransCtx {funcCtx = MLispFuncCtx {regStackTop = 0, funcCode = Data.Vector.fromList [Irmovs {strVal = "vector-set", toReg = 0},Mrmov {fromVarReg = 0, toReg = 0},Irmovs {strVal = "exampleVec", toReg = 1},Irmovi {intVal = 2, toReg = 2},Irmovs {strVal = "+", toReg = 3},Mrmov {fromVarReg = 3, toReg = 3},Irmovi {intVal = 1, toReg = 4},Irmovi {intVal = 2, toReg = 5},Call {nArgs = 2, baseReg = 3},Call {nArgs = 3, baseReg = 0}], funcId = 0, nextLocalId = 1}, funcStack = [], labelOffsetMapping = Data.Map.fromList [], code = Data.Vector.fromList [], codeLen = 0, nextFuncId = 1}
expectedCtx8 = MLispTransCtx {funcCtx = MLispFuncCtx {regStackTop = 0, funcCode = Data.Vector.fromList [Imkfunc {nArgs = 2, MLisp.Compiler.label = (1,0), toReg = 0},Irmovs {strVal = "example", toReg = 1},Rmmov {fromReg = 0, toVarReg = 1}], funcId = 0, nextLocalId = 1}, funcStack = [], labelOffsetMapping = Data.Map.fromList [((1,0),0)], code = Data.Vector.fromList [Irmovs {strVal = "example", toReg = 3},Rmmov {fromReg = 0, toVarReg = 3},Irmovs {strVal = "arg1", toReg = 3},Rmmov {fromReg = 1, toVarReg = 3},Irmovs {strVal = "arg2", toReg = 3},Rmmov {fromReg = 2, toVarReg = 3},Irmovn {toReg = 4},Ret {retValReg = 4}], codeLen = 46, nextFuncId = 2}
expectedCtx9 = MLispTransCtx {funcCtx = MLispFuncCtx {regStackTop = 0, funcCode = Data.Vector.fromList [Irmovi {intVal = 1, toReg = 0}], funcId = 0, nextLocalId = 1}, funcStack = [], labelOffsetMapping = Data.Map.fromList [], code = Data.Vector.fromList [], codeLen = 0, nextFuncId = 1}
expectedCtx10 = MLispTransCtx {funcCtx = MLispFuncCtx {regStackTop = 0, funcCode = Data.Vector.fromList [Irmovi {intVal = 1, toReg = 0},Irmovi {intVal = 2, toReg = 0}], funcId = 0, nextLocalId = 1}, funcStack = [], labelOffsetMapping = Data.Map.fromList [], code = Data.Vector.fromList [], codeLen = 0, nextFuncId = 1}
expectedCtx11 = MLispTransCtx {funcCtx = MLispFuncCtx {regStackTop = 1, funcCode = Data.Vector.fromList [Irmovi {intVal = 1, toReg = 0}], funcId = 0, nextLocalId = 1}, funcStack = [], labelOffsetMapping = Data.Map.fromList [], code = Data.Vector.fromList [], codeLen = 0, nextFuncId = 1}
expectedCtx12 = MLispTransCtx {funcCtx = MLispFuncCtx {regStackTop = 1, funcCode = Data.Vector.fromList [Irmovc {charVal = 'a', toReg = 0}], funcId = 0, nextLocalId = 1}, funcStack = [], labelOffsetMapping = Data.Map.fromList [], code = Data.Vector.fromList [], codeLen = 0, nextFuncId = 1}
expectedCtx13 = MLispTransCtx {funcCtx = MLispFuncCtx {regStackTop = 1, funcCode = Data.Vector.fromList [Irmovs {strVal = "foo", toReg = 0}], funcId = 0, nextLocalId = 1}, funcStack = [], labelOffsetMapping = Data.Map.fromList [], code = Data.Vector.fromList [], codeLen = 0, nextFuncId = 1}
expectedCtx14 = MLispTransCtx {funcCtx = MLispFuncCtx {regStackTop = 1, funcCode = Data.Vector.fromList [Irmovb {boolVal = True, toReg = 0}], funcId = 0, nextLocalId = 1}, funcStack = [], labelOffsetMapping = Data.Map.fromList [], code = Data.Vector.fromList [], codeLen = 0, nextFuncId = 1}
expectedCtx15 = MLispTransCtx {funcCtx = MLispFuncCtx {regStackTop = 1, funcCode = Data.Vector.fromList [Irmovn {toReg = 0}], funcId = 0, nextLocalId = 1}, funcStack = [], labelOffsetMapping = Data.Map.fromList [], code = Data.Vector.fromList [], codeLen = 0, nextFuncId = 1}
expectedCtx16 = MLispTransCtx {funcCtx = MLispFuncCtx {regStackTop = 1, funcCode = Data.Vector.fromList [Irmovs {strVal = "val", toReg = 0},Mrmov {fromVarReg = 0, toReg = 0}], funcId = 0, nextLocalId = 1}, funcStack = [], labelOffsetMapping = Data.Map.fromList [], code = Data.Vector.fromList [], codeLen = 0, nextFuncId = 1}
expectedCtx17 = MLispTransCtx {funcCtx = MLispFuncCtx {regStackTop = 1, funcCode = Data.Vector.fromList [Irmovs {strVal = "+", toReg = 0},Mrmov {fromVarReg = 0, toReg = 0},Irmovi {intVal = 1, toReg = 1},Irmovi {intVal = 2, toReg = 2},Call {nArgs = 2, baseReg = 0}], funcId = 0, nextLocalId = 1}, funcStack = [], labelOffsetMapping = Data.Map.fromList [], code = Data.Vector.fromList [], codeLen = 0, nextFuncId = 1}
expectedCtx18 = MLispTransCtx {funcCtx = MLispFuncCtx {regStackTop = 1, funcCode = Data.Vector.fromList [Imkfunc {nArgs = 1, MLisp.Compiler.label = (1,0), toReg = 0}], funcId = 0, nextLocalId = 1}, funcStack = [], labelOffsetMapping = Data.Map.fromList [((1,0),0)], code = Data.Vector.fromList [Irmovs {strVal = "arg", toReg = 2},Rmmov {fromReg = 1, toVarReg = 2},Irmovi {intVal = 1, toReg = 2},Ret {retValReg = 2}], codeLen = 18, nextFuncId = 2}
 
