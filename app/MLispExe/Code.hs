module MLispExe.Code where

import Data.Maybe (fromJust)
import Data.ByteString as BS (
    ByteString,
    pack,
    getContents,
    readFile,
    splitAt,
    foldr,
    take,
    drop,
    putStr,
    writeFile,
    append,
    length)
import Data.ByteString.Char8 as BSChar (pack, unpack)
import System.Exit (exitFailure)
import System.IO (IOMode(..), hPutStr, stdout, openFile, hClose)
import Numeric (showHex)

import MLisp.Util
import MLisp.VM (MLispVM(..), MLispRuntimeError(..), runOp, runVM, makeVM)
import MLisp.Parser (Statement, parseProgram)
import MLisp.Compiler (MLispTransOp, transformAST, getCode, genByteCode, getEntryPoint, evalTransOp, makeTransCtx)

-- [ Constants ]
-- Bytecode magic string
byteCodeMagic :: ByteString
byteCodeMagic = BS.pack [0xde, 0xad, 0xbe, 0xef, 0x01]

-- [ Functions ]
-- Translate code
translate :: Statement -> Bool -> MLispTransOp (ByteString, Int)
translate ast runMain = do
    transformAST ast runMain
    -- Debug only
    insts <- getCode
    -- Return code and entry point
    byteCode <- genByteCode
    entryPoint <- getEntryPoint
    return (byteCode, entryPoint)

-- Legalize characters
legalizeChars :: Char -> Char
legalizeChars c = if c `elem` [' '..'~'] then c else ' '

-- Compile code
compileCode :: String -> IO (ByteString, Int)
compileCode rawInput =
    -- Character filtering
    let input = fmap legalizeChars rawInput
    in case parseProgram input of
        (Left err) -> do
            putStrLn ("Error: Parse error: " ++ err)
            exitFailure
        -- Transform AST and get bytecode
        (Right ast) -> case evalTransOp (translate ast True) makeTransCtx of
            (Left err) -> do
                putStrLn ("Error: AST transform error: " ++ show err)
                exitFailure
            (Right result) -> return result

-- Handle runtime error
handleVMError :: MLispRuntimeError -> MLispVM -> (String -> IO ()) -> IO ()
handleVMError err vm writer = case err of
    -- Unknown opcode
    (UnknownOpcode { opcode = op }) ->
        writer ("Error: Unknown opcode 0x\"" ++ showHex op "\".\n")
    -- No local scope
    NoLocalScope -> writer "Error: Local scope does not exist.\n"
    -- Program counter out of range
    CounterOutOfRange -> do
        let addr = counter vm
        writer ("Error: Program counter out of range. (" ++ showHex addr ")\n")
    -- Empty call stack
    EmptyCallStack -> writer "Error: Empty call stack. Cannot return.\n"
    -- Variable not found
    (VariableNotFound { varName = name }) ->
        writer ("Error: Variable \"" ++ name ++ "\" not found.\n")
    -- Call arguments mismatch
    (CallArgsMismatch { expectNArgs = nArgs1, actualNArgs = nArgs2 }) ->
        writer ("Error: Expect " ++ show nArgs1 ++ " arguments, got " ++ show nArgs2 ++ ".\n")
    -- Type mismatch
    (TypeMismatch { expectType = type1, actualType = type2 }) ->
        writer ("Error: Expect " ++ type1 ++ " type, got " ++ type2 ++ " type.\n")
    -- Integer required
    IntegerRequired -> writer "Error: Integer required for operation.\n"
    -- HFFI error
    (HFFIError { message = msg }) -> writer ("Error: HFFI error: " ++ msg ++ "\n")

-- Parse bytecode file
parseByteCodeFile :: ByteString -> (ByteString, Int)
parseByteCodeFile input = (byteCode, entryPoint)
    where (entryPointData, byteCode) = BS.splitAt 4 input
          entryPoint = BS.foldr buildInt 0 entryPointData

-- Read input from standard input or file
readInput :: String -> IO ByteString
readInput inputFile = if inputFile == "-" then BS.getContents else BS.readFile inputFile

-- Write output
writeOutput :: String -> ByteString -> IO ()
writeOutput outputFile = if outputFile == "-" then BS.putStr else BS.writeFile outputFile

-- Run file
runFile :: String -> String -> IO ()
runFile inputFile outputFile = do
    -- Read input
    input <- readInput inputFile
    -- Detect file type: source or bytecode?
    let magicLen = BS.length byteCodeMagic
    (byteCode, entryPoint) <- if BS.take magicLen input == byteCodeMagic
        -- Bytecode file
        then return $ parseByteCodeFile $ BS.drop magicLen input
        -- Source file
        else compileCode $ unpack input
    -- Output writer
    handle <- if outputFile == "-"
        then return stdout
        else openFile outputFile WriteMode
    let writer = hPutStr handle
    -- Run code
    (result, newVM) <- runOp runVM makeVM {
        program = byteCode,
        counter = entryPoint,
        outputWriter = writer
    }
    -- Handle VM error
    case result of
        (Left err) -> handleVMError err newVM writer
        (Right _) -> return ()
    -- Close output file
    hClose handle

-- Compile file
compileFile :: String -> String -> IO ()
compileFile inputFile outputFile = do
    -- Read input
    input <- readInput inputFile
    -- Compile code
    (byteCode, entryPoint) <- compileCode $ unpack input
    -- Write code to file and standard output
    let entryPointData = BS.pack $ fmap fromIntegral $ makeInt32 entryPoint
    let output = byteCodeMagic `append` entryPointData `append` byteCode
    writeOutput outputFile output

-- Print file AST
printFileAST :: String -> String -> IO ()
printFileAST inputFile outputFile = do
    -- Read input
    rawInput <- readInput inputFile
    let input = fmap legalizeChars $ unpack rawInput
    -- Parse program
    case parseProgram input of
        (Left err) -> putStrLn ("Error: Parse error: " ++ err)
        -- Transform and show AST
        (Right ast) -> writeOutput outputFile $ BSChar.pack $ show ast
