module Main where

import System.Environment (getProgName, getArgs)
import System.Exit (exitSuccess, exitFailure)

import MLispExe.REPL (runREPL)
import MLispExe.Code (runFile, compileFile, printFileAST)

-- [ Types ]
-- MLisp interpreter arguments
data MLispInterpArgs = MLispInterpArgs {
    -- Source / bytecode file
    inputCode :: Maybe String,
    -- Print AST file
    printASTFile :: Maybe String,
    -- Output file
    outputResult :: String
}

-- MLisp compiler arguments
data MLispCompilerArgs = MLispCompilerArgs {
    -- Source file
    inputSource :: String,
    -- Output file
    outputByteCode :: String
}

-- [ Functions ]
-- Parse flag argument
parseFlagArg :: String -> [String] -> IO (String, [String])
-- Argument not specified
parseFlagArg flag [] = do
    putStrLn ("Error: Expect input after \"" ++ flag ++ "\".")
    exitFailure
-- Return argument and remain arguments
parseFlagArg _ (arg:remain) = return (arg, remain)

-- Parse compiler arguments
parseCompilerArgs :: [String] -> IO MLispCompilerArgs
parseCompilerArgs args = case args of
    -- No more arguments
    [] -> return MLispCompilerArgs { inputSource = "-", outputByteCode = "-" }
    -- Help
    ("-h":_) -> do
        putStrLn "kc: MLisp bytecode compiler."
        putStrLn "Usage: kc [-h] [<Input>] [-o <Output>]"
        exitSuccess
    -- Output file
    ("-o":remain1) -> do
        (file, remain2) <- parseFlagArg "-o" remain1
        -- Parse remaining arguments
        state <- parseCompilerArgs remain2
        return state { outputByteCode = file }
    -- Input file
    (file:remain) -> do
        state <- parseCompilerArgs remain
        return state { inputSource = file }

-- Parse interpreter arguments
parseInterpArgs :: [String] -> IO MLispInterpArgs
parseInterpArgs args = case args of
    -- No more arguments
    [] -> return MLispInterpArgs {
            inputCode = Nothing,
            printASTFile = Nothing,
            outputResult = "-"
        }
    -- Help
    ("-h":_) -> do
        putStrLn "ki: MLisp source code / bytecode interpreter."
        putStrLn "Usage: ki [-h] [-repl] [-i <Input>] [-t <Print AST Input>] [-o <Output>]"
        exitSuccess
    -- REPL mode
    ("-repl":_) -> do
        runREPL
        exitSuccess
    -- Input file
    ("-i":remain1) -> do
        (file, remain2) <- parseFlagArg "-i" remain1
        -- Parse remaining arguments
        state <- parseInterpArgs remain2
        return state { inputCode = Just file }
    -- Print AST file
    ("-t":remain1) -> do
        (file, remain2) <- parseFlagArg "-t" remain1
        -- Parse remaining arguments
        state <- parseInterpArgs remain2
        return state { printASTFile = Just file }
    -- Output file
    ("-o":remain1) -> do
        (file, remain2) <- parseFlagArg "-o" remain1
        -- Parse remaining arguments
        state <- parseInterpArgs remain2
        return state { outputResult = file }
    -- Unknown argument
    (unknown:_) -> do
        putStrLn ("Error: Unknown argument or flag \"" ++ unknown ++ "\".")
        exitFailure

-- Get MLisp arguments
getMLispArgs :: IO (String, [String])
getMLispArgs = do
    -- Get program name and arguments
    progName <- getProgName
    args <- getArgs
    -- Program name
    if progName /= "mlisp"
        then return (progName, args)
        else case args of
            -- No command name specified
            [] -> do
                putStrLn "Error: No command name specified."
                putStrLn "See \"mlisp kc -h\" or \"mlisp ki -h\" for more information."
                exitFailure
            -- Extract command name from arguments
            (cmdName:cmdArgs) -> return (cmdName, cmdArgs)

-- Program arguments
main :: IO ()
main = do
    -- Get MLisp arguments
    (cmdName, cmdArgs) <- getMLispArgs
    -- Do parsing
    case cmdName of
        -- Interpreter
        "ki" -> do
            args <- parseInterpArgs cmdArgs
            case (inputCode args, printASTFile args) of
                -- Run file
                (Just file, _) -> runFile file $ outputResult args
                -- Print AST
                (_, Just file) -> printFileAST file $ outputResult args
                -- No operation specified
                _ -> do
                    putStrLn "Error: No interpreter operation specified."
                    exitFailure
        -- Compiler
        "kc" -> do
            args <- parseCompilerArgs cmdArgs
            compileFile (inputSource args) $ outputByteCode args
        -- Illegal commands
        _ -> do
            putStrLn ("Error: No such command " ++ cmdName ++ ".")
            exitFailure
